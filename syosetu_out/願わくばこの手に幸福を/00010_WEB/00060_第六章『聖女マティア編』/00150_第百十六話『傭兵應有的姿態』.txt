夜風吹過，酒館中的蠟燭搖曳著。

為了保證光亮，準備了好幾根蠟燭，搖曳的光影照亮了布魯達的臉。

「你想雇傭我？饒了我吧，有什麼意義，這和養一條野狗可不一樣。卡、哈哈哈！」

如果想雇傭傭兵，那就該在這腐臭的城市中尋找吧，布魯達的嘴唇在動著。在我請他喝了一杯朗姆酒之後，布魯達就開始變得活躍了。早知道一開始就那麼做了。

聽到布魯達的問話，我哼了一聲，眯起了眼睛。
的確，這對布魯達來說應該很難理解。即使要雇傭傭兵，也想不出特意選擇醉酒後在後街挨揍然後留下青痣的他的理由。

怎麼說呢。

哎呀，也不能把來龍去脈全部告訴布魯達吧。如果做那樣的事，會讓本來就有所疑惑的他變得更加困惑的吧。

慎重地選擇語言，張開嘴。

「⋯⋯我想拜託你做只能由你來的工作，布魯達。是的，這是無法委託他人的工作。」

工作地點就在貝爾菲因。我一邊和布魯達對視一邊補充到

布魯達眯起眼睛，用手指玩弄著帽子的邊緣。那就是，這個男人沉浸於思考時的表現。
這個叫布魯達的男人，一方面對於酒很沒有品味，另一方面也具備著奇怪的深思熟慮。這種情形實在是扭曲，每天為酒費所困擾的傢伙，明明大多只要一有工作，不管散發著多麼可疑的氣味，都會像餓極的野狗一樣撲上去。

因此，他的這種思索，似乎與他的存在方式不相稱。

「──說得好聽，能讓我心情變好。但是不行啊。看不出非本大爺不可的理由，只能聽出是性質惡劣的玩笑。」

酒也不錯，布魯達一邊這麼說，一邊把喝乾的酒放在桌子上。然後就那樣直起了腰，表示談判已經結束。

連委託的內容都還沒說吧，怎麼說呢，該說是警惕性強呢，還是該說是性急呢？不過，這點也是他的特徵。

在與傭兵的交涉中，無論哪一方離開桌子，都宣告著談判破裂。追逐想要離去的人，既粗俗又違反禮儀，這就是這個城市的慣例。
我也不能阻止打算離開的布魯達。即使想追加點酒，他被人請酒時也只會喝一杯。

因此，我並沒有打算阻止離開酒館的那道身影，只是打了招呼。

「就先到這裡吧，等你的心情、天氣變好了再來也行。」


◇◆◇◆

真是個奇怪的男人，布魯達扭扭頭，不僅請自己喝酒，還說想雇傭我，真是個奇怪的男人。
布魯達充分理解自己的能力和評價。

只曉得喝酒的蠢材。下級傭兵。用針頭表演雜技是拿手絶活的男人。這是貝爾菲因對布魯達的評價。布魯達理解那些評價相當公正。

正因為如此，越發不明白了。到現在為止會請自己喝酒的人雖然有幾個，但是作為傭兵的布魯達什麼的，從來沒有過。說起工作的話，好像大多數時候都是用來湊數，倚仗自己戰力什麼的未曾有過。

那個人想雇傭我什麼的，果然是個性質惡劣的笑話啊，布魯達用食指玩弄帽子的邊緣。

當然，並不會不舒服。雖說只是為了能在這個城市生存下去，但布魯達的身份確實是傭兵，有人需要作為傭兵的自己的力量而會損害到自己心情的人是不存在的吧。即使那本事真的不能算是優秀。

但是，布魯達堅信事出反常必有妖。
奇事必有內幕，背後常有危險。所謂有內幕的人，就等於帶來危險。根本不值得信賴。

布魯達認為，信賴是相互走近的同時，一邊互相開著玩笑一邊培養的東西。
大部分傭兵都不會像這樣進行判斷。對於傭兵來說，重要的是報酬是否與風險相符。

不管委託背後是否有所隱瞞，只要覺得風險低於報酬，就會欣然答應。這就是所謂傭兵的生存方式──以自己的生命為代價度過每一天。
這樣想的話，既可以說布魯達的判斷方法非常慎重，但以傭兵的身份來說的話可以說是非常膽小了。

突然，漫無目地的動著的腳停了下來。布魯達的視線投向了前方──傭兵專用的公會。雖然和冒険者使用的正式公會意義不同，但是誰都因其性質而稱呼那為傭兵公會。
委託傭兵當然也會有個人間的契約，但大部分都是通過貝爾菲因官方管理的公會進行委託的。

所謂的傭兵，本來就是管理起來很辛苦的存在。任何人都是無根的草。有奶就是娘，不會固定在什麼地方進行活動。如果幹不下去了就會立馬從傭兵轉職成野盜，是麻煩的打工者這種立場。

但是，那是在貝爾菲因之外的地方。
貝爾菲因是傭兵的後盾，作為城市的「商品」，將固定的傭兵置在身邊是很有必要的，那麼，重要的就是管理方法了。

如果工作合同全部是私下簽訂的，即使只是從成功報酬中收取少許，也很難掌握明確的狀況。如果可以的話，想集中管理合同，如稅收一般收取費用。
出於這種考慮創造出來的，就是傭兵公會這個組織。

商人把必要的要求告訴公會，公會根據商人的請求招集傭兵。有時會使用公會附帶的傭兵，有時也會招募像布魯達那樣的閑散傭兵。
結果，商人可以省去與傭兵直接交涉的工夫，作為傭兵，至少不用擔心對方貪掉報酬，自己的辛苦白費。從某種意義上來講，這是一種良性循環。

雖說如此，本就不多的報酬還要被公會分掉一部分，對於活一天算一天的傭兵來說，實在是難以忍受。
這是布魯達平時不願意進入的場所，但因為今天喝了酒，感覺還不錯，那就去打趣一下吧。歡快地吹著口哨，把手搭在門上。

「喂喂，稀客，是喝多了嗎？先說好，現在沒有能交給你的工作。」

剛一進門，公會老板就把這句話給扔出去了。半開玩笑的性質。

布魯達一邊發出奇怪的聲音，一邊邁著搖搖晃晃的像醉了一樣的腳步，坐在附近的椅子上。

「什麼啊，最近伽羅亞瑪利亞不是淪陷了嘛，商人們這時候都還在觀望吧？呆在暫時閑下來的公會中，應該不至於被趕出去吧。」

我看著老板將張貼在公會正面的羊皮紙取下。那張羊皮紙是蓋有大聖堂印章的指令，本來，沒有許可是不允許拆除的。

「啊，哈哈。老板，要改變信仰的話就趕緊收買一下我吧，只要老子先跑到大聖堂那去告密，就能把金幣弄到手了。」

老板一邊說著「蠢蛋」，一邊把揉成一團的羊皮紙扔到布魯達的手邊。自然而然地，羊皮紙的卷曲狀態逐漸被解開，上面記載的內容逐漸顯現出來。

「領主的命令。暫時不能把那東西貼著。」

說貼就貼，說取就取，太任性了。老板嘟噥著吐出牢騷。

老板的言語傳入耳朵的同時，布魯達的眼內也映出了一篇文章。

──虔誠的教徒們。判處魔女瑪蒂婭與大罪人路易斯死刑。

「報酬是⋯⋯從來沒見過的面額啊。不知道值幾杯朗姆酒。嗯，不錯的笑話。」

布魯達好像覺得很有趣似地眨著眼睛，微微抬起臉頰。