那一天，在距離較遠的山路上發現了朝著傑巴古而來的入侵部隊。

由於這件事是預料中事，因此迅速的利用傳話魔法將相關的報告送到了瑪內夏去。
敵軍數量有數萬人。
穿過山路而來的部隊，其數量相較於攻打魔法都市時少了很多，但這樣的數量也已經足夠用來攻陷傑巴古了。
而且其中還有一架塗成緋紅色的巨大機器人坐鎮。

「這個還真讓人頭痛阿⋯」

負責指揮傑巴古軍隊的巴爾加斯，其內心正抱著頭煩惱著。
從莉雅那傳來了『避免由我方開啟戰端』的這個傳話。但即使無視莉雅的勸告，這邊也不會主動出擊。
另外魔族軍隊則是開始搭設營地，僅僅只是將這座城市包圍起來而已。

第二天，莉雅與卡拉從瑪內夏飛到這邊，就跟字面的意思一樣，是用飛行的方式飛過來。
接著等到了傍晚時分，魔族軍那派出了使節過來。
其中包含了那個有著亞麻色頭髮的吸血鬼少女明日香的身影。

「真抱歉阿，選這種時間」

從明日香口中得到的第一句話就是道歉。因為吸血鬼是無法在白天行動的，就算明日香本人是很強大的吸血鬼也一樣。
在魔族軍營地與傑巴古大門之間正中央的位置搭設起了帳篷。
奧古斯這邊由莉雅及卡拉率領數名幕僚，而魔族軍則是由明日香及數名武官組成。

「在談話之前首先有件關於三年前的事情想問。當初那個狼人男到底是派來做什麼的？」

莉雅的聲音裡充滿著怒氣，畢竟自己最重要的人被他傷害了，這邊如果不感到生氣反而比較奇怪。

「那個阿⋯是我們這邊的錯。我們這邊雖然也有派人監視但卻被甩開了⋯但至少陛下在最緊要的關頭勉強有趕上」

阻止莉雅揮下刀子的確實是魔王，照那個時機點來看，的確是能趕的上幫助卡拉沒錯。

但是莉雅無法接受。

「說實話我無法決定要不要信賴飼養出那種人的魔王」
「雖然我也覺得那種人早點去死比較好⋯不過他對於陛下的忠誠心是實實在在的，而且就算是那種人也是有家庭的」

是嗎，有家人在阿。
就算是那種人竟然也會有家人阿。
如今有了小孩的莉雅，對於殘留下來的家族至少還能保有一些哀悼之意。

「似乎有１３個老婆以及２５個小孩的樣子？」

更正，不值得哀悼。

「那是甚麼鬼阿，是有那麼喜好女色嗎」

但這麼說的莉雅自己也有三個老婆及四個孩子。
而她背後的卡拉，表情則完全沒有改變。

───

「那麼，你們到底有什麼要求？」

身為當事人的卡拉將話題拉了回來。對她本人來說那已經是過去的事了。
聽到她的話後，明日香的表情一下子鬆懈了下來。

「我們並不打算與人類開戰」

這似乎是實話。卡拉偷偷施展了檢測謊言的魔法也顯示著同樣的結果。雖然還是有隱藏某種目的的可能性。

「我們想要那座山以及周圍的土地，我們會在這耕作並建立城市」

這也不是謊言。因為如果是這個吸血鬼少女的話，只要一說謊馬上就可以從她的臉色看出來。

「進一步希望能承認我等與人類共生存這點」
「嗚嗯⋯⋯」

莉雅在思考著。與魔族共生存這件事在過去三年裡一直徘徊在莉雅的腦中。
卡拉那件是姑且忽略的話，冷靜思考一下就很清楚，一旦在這與他們為敵進行戰鬥的話，會造成非常大的損害。
而且有個非常讓人在意的一件事。

「魔王現在人在哪裡？」

這是最重要的情報。
如果他人在附近觀察這場會談的話，一旦談判破裂莉雅或卡拉被迫與魔族軍戰鬥時，他恐怕會趁機襲擊過來吧。而屆時消耗很大的兩人恐怕難以與之為敵吧。
萬一真是這樣的話姑且不論莉雅個人的好惡，只能暫時接受魔族的要求了。

「這個嘛⋯連我也不知道」

聲音變的微弱起來。
但是。

「但是他一定會待在需要他的地方，陛下就是這種人」

跟她微弱的語氣相反，話中充滿了對魔王的信賴。

莉雅沒有太多選項可以選。
大體來說要避免戰鬥。
如果選擇戰爭的話就很單純了。魔族軍就算將眼前這個還算熟識的吸血鬼算在內，只要莉雅與卡拉聯合了傑巴古的軍隊，仍一定能夠獲勝的。但是也確實會造成非常大的犧牲。
而莉雅是不容許這種犧牲存在的。

「要我認同建設城市這點也行，但是必需要在對我方有利的情況下進行」

明日香沉默的催促著莉雅繼續說下去。

「首先，不允許建設城牆。但為了預防萬一發生需要防衛城市的情況，可以允許建設柵欄等級的防衛設施」
「這個的話，可以喲」
「也可以給你們土地，但必須距離傑巴古一定距離以上，這也是為了萬一有必要時，可以讓傑巴古有時間準備防守」
「這個也沒問題」

莉雅與明日香看起來很平常，但他們身後的部下們則緊張的半死。

「最後這個可能是最麻煩的一點吧⋯」

莉雅凝視的明日香，那強烈的視線不允許對方說謊。

「在我方確認你們的存在是安全以前，希望你們能留個人質在我方」

明日香一邊嘆了口氣一邊點頭稱是。

「我知道了。那就我跟其他幾個人吧，因為我們比較弱所以就交給你們吧」
「不，不用你擔任人質，因為你太強了」

這倒是大大的出乎明日香的意料之外。

「人質的人選就⋯你背後站的那幾個人如何？」
「欸、不、這會造成困擾阿，因為軍隊少了他們很難指揮」

雖然也有豬頭人及哥布林在場，不過他們都只是隨侍的工作人員吧。

「不然還是由妳來吧，畢竟妳白天很弱」

───

就這樣，大陸曆３００３年，奧古斯與魔族軍在傑巴古這個地方簽訂了互不侵犯條約。條約本身的期限只有一年，但是在雙方都同意的情況下可以延長年限。

魔族有許多強而有力的種族，他們很快就在距離傑巴古一天路程的地方建立起聚落了。
同時包圍著傑巴古的魔族軍隊也展開了移動，來到了莉雅指定的位置開始進行耕作。
而這點莉雅是自己親自到各地用自己的眼睛進行確認。

過沒多久就有商隊從傑巴古朝魔族的聚落出發。
在一開始的時候這些商隊一定會讓莉雅或卡拉隨行，這是為了預防萬一發生狀況時，可以確保商隊保有一定的震攝力。

但真的什麼事都沒有發生。
魔族們只是安靜的耕耘著，他們利用人類所不知道的知識，在那片被人類視為不毛之地的土地上讓作物一一發芽了。

待在傑巴古的明日香也表現的很老實，偶爾會在有人監視的情況下上街閒逛。
不過這只限制於白天的時段，因為這段時間是吸血鬼力量最為薄弱的時候。

───

就這樣平靜的日子一天天過去。
此時突然有個消息從瑪內夏傳達過來。
薩基回到國內了。

最開始聽到這個消息的時候，莉雅的臉上只有著純粹的喜悅。
但是聽到薩基要求莉雅、卡拉以及吉妮維亞必須儘快聚集起來開會後，讓莉雅的臉上充滿著懷疑。
因為薩基之前都有定期用信件連絡，也有派連絡員幫忙傳話。
因此這個緊急要求帶給了莉雅不好的預感。

「明日香，可以一起來一趟嗎？」
「我是無所謂阿，但妳那邊沒關係嗎」
「這是因為只留妳一個人在這太危險了」

明日香專用的機器人，目前正停放在傑巴古城內的某個帳篷裡。

如果萬一讓她駕駛那個進行戰鬥的話，光憑那台機器人就足以毀滅傑巴古了。
雖然莉雅相信明日香所作出的承諾，但為政者必須將所有可能性都考慮進去。

───

莉雅、卡拉以及明日香三個人，利用飛行緊急的趕回瑪內夏。
當他們抵達王宮中庭的時候，一個熟悉的臉孔出現在那裡。

「歐捏醬！」

不過令人熟悉的只有臉而已。
薩基的身高如今已經遠超過莉雅了。

「你這傢伙長的還真大阿」

很明顯的超過１７０公分，雖然莉雅在女性中也算是個子高的，但她的身高還不到１７０。

「我有一大堆想說的事阿，不過這邊⋯⋯要讓明日香一起聽阿？」

如今太陽早已西沉，連殘餘的光芒都沒剩了，若非如此明日香是沒辦法在天空中飛的。

「如果我的存在會造成困擾的話，那我就去找個地方逛逛好了」
「不會⋯倒不如說我這邊也很想知道魔族的人們到底知道了多少真相」

連同吉妮維亞在內的五個人一起來到了城中的接待廳。
薩基為了避免被偷聽甚至仔細的進行了消音作業，讓其他四個人感到有些不可思議。

「我該從何談起比較好呢⋯⋯」

薩基給人感覺似乎有些迷網，而他的表清已經看不見以往那份稚嫩，相對的讓人感到非常的老成。

「就先說說結論吧，要談的事情之後再說」

莉雅開口催促著，她那完全沒變的率直個性讓薩基感到很高興。

「那、我說了」

即使如此薩基仍吞了一口口水後才開口。

「這樣下去人類⋯不對，是這個世界會毀滅」

而這正是大崩壊的真相。