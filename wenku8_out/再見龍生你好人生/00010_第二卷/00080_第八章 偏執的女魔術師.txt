雖然我們只有在休息的時間裡才被放出馬車，但能夠通過安設在車體上較高位置的小四方形窗戶來確認周圍的風景變化，倒也還好過。
而且在一路上，不僅僅是帕拉祢絲司祭，管理官的從士君也在各方面關照著我們，盡可能地為我們提供方便，可以說是意外的幸運。
另一方面，雖然賽莉娜一天裡基本上都是在馬車裡和我緊貼在一起，但她的不安正在漸漸地消散，開朗的神色出現的次數開始增加。現在也是，賽莉娜一副對車外抱有興趣的模樣，稍許地伸了伸腰，透過窗戶處看著外面的風景。
這樣一來，她的姿勢就變成了小屁屁朝向我，但賽莉娜似乎並沒有察覺到這一點。

『是在誘惑我麼？』

雖然是這麼想了，但從賽莉娜的性格來看，十有八九不是有意的吧。
也許是因為她是拉米婭的原因吧，畢竟那個種族的本能之一便就是誘惑異種族雄性。雖然賽莉娜有意地誘惑雄性的能力是慘不忍睹的，但在本人無意識的時候那樣做後⋯⋯

（果然是拉米婭吶啊）

不知道為何，我的心情變得不可思議起來，就跟實感到了自己女兒的成長後的父親一樣。

「嗚～嗯，雖然已經到了舖設得很好的路上了後搖晃的幅度也變小了，但總感覺好像是往郊外的方向一樣？是因為不可以把拉米婭帶進街市中的原因嗎？」
「或許是郊外也有適當的設施，但這應該也是那位想要會見賽莉娜的魔法使殿下的原因才去的吧吶」

在除了高達和帕拉祢絲司祭之外，還有一個人找到了我們的晚上，我們得知了賽莉娜被請到伽羅瓦的原因並不僅僅是為了辦理移住的手續。
經過歐理維爾向伽羅瓦總督府報告進而傳達向王国的恩特之森的事變，給予了總督府相當大的衝擊。在擊退侵略者的魔兵中大活躍了的克里斯汀娜自然不用說，我和賽莉娜似乎也成了他們所關注的對象。
要將分類為魔物拉米婭的賽莉娜給危險視的聲音當然有著，但她有著與魔兵們勇猛交戰了的實績，且對人類極為友好的態度以及來自瑪依拉爾的神諭也有著，導致伽羅瓦的上層部把話題推進向了賽莉娜被邀請到了貝倫村的事情上。
但是在那之前，由於某位魔法使表示自己對賽莉娜很感興趣，希望能夠與賽莉娜會面的原因，我們現在正在被帶往去那位魔法使所在的地方去。

「啊，馬路的對面有像是迎接的人過來了呢」

我將魔力通過雙眼，進行了魔眼化後，看向賽莉娜所望著的同一方向，確實發現了十名左右騎著馬的武裝男子們。
隨著愈加接近這邊而放緩了馬速的他們，向著高達所乘坐的馬車走去，到了一定距離後，從懷中取出一封封書，遞到了從馬車裡出來了的從士君手中。

「好像這後面是跟他們走的樣子吶」
「話又說回來，那位魔法使桑到底對我有怎樣的興趣啊？」
「我也不想讓賽莉娜妳不安，但從管理官的話來看，我感覺應該不是什麼好興趣吶」
「嗚嗚???，果然」
「雖然說是和克里斯汀娜桑一起，但能擊退大惡魔級敵人的拉米婭，終歸還是少見的吧吶。多少是會產生一點興趣的吧。
放心，不管Ta在打著什麼鬼主意，我也不會讓Ta碰賽莉娜妳一根手指頭的」
「誒嘿嘿，多蘭桑這麼說了的話，就什麼也不用擔心了的說呢！」

對那樣說道後開朗地笑著的賽莉娜，我也以微笑回應她。
在我們等了一段時間後，馬夫和周圍的人開始交棒換人，帶著我們向郊外道路的前往走去。
雖然來接這邊的那些人一句招呼都沒打，但他們也沒有敵意相向，僅僅是圍在馬車周圍，完成自己的任務。

（Fumu⋯⋯）

在這之後，我們被運向聳立在只有森林動物或精靈存在般的森林之中的公館。雖說是郊外，但其實離伽羅瓦並不是非常的遠。
馬車一停下來時，我們便就被面色很是蒼白的馬夫催促，然後被帶路到公館之中。
不分男女老少，凡是進入視線內的傭人，誰都是一副蒼白的臉色，總感覺沒什麼精氣的樣子。然後是裝飾在公館內的繪畫或是雕像一類，大多數都是注入了魔力在裡面的魔法物品。
雖然知道招我們來的魔法使在附屬總督府的所有魔法使中也是位居相當高位的人物，但即便如此，我仍然覺得其生活有稍許的豪華過頭了。
當然，對方應該是高收入一列的吧，但即便是其還兼任著魔法公會的官員，也依舊難以想像其之收入能讓其如此生活。
是副職業從事著魔術關聯的買賣，或是還開著私塾呢？還是說染指有什麼不正當的事物呢⋯⋯

可悲的是，從我前世的經驗來看，可能性位居第一是最後的那個原因。
在與賽莉娜年齡相仿的女僕和青年馬夫兩者的帶路下，我們同對賽莉娜持有興趣、附屬總督府的魔法使祁蓮會面了。
在我們被帶到的客廳內，有一張擺滿了散發著芬芳香氣的紅茶與色彩鮮艷的各類茶點的桌子，看上去三十歳左右的女性魔法使祁蓮就靜靜地坐在桌子對向我們的那一端。
濃紫色的頭髮分為三分，用通紅的髮帶束在一起，有些容易垂下的綠眼內佔據滿了好奇的光芒，身穿著的禮服宛若染血一般赤紅，上面裝飾著的絢爛寶石群閃閃發光，面容上的笑容看上去和藹可親。

「我勉強高達管理官把妳帶過來了，不知是不是給妳帶來了多餘的不安了？
真是抱歉呢。從以前開始，一旦碰上有興趣的事情後就變得看不清周圍的事情了吶我」

祁蓮僅說了這些，然後端起了自己的茶杯，將之移至塗著純黑色口紅的唇前。

（明明只是一杯飲品而已，但實際上卻是要支付不知多少枚的硬幣才能喝到的高極品麼？）

我這麼想著，看來我的價值觀也變成了人類的了吶。
賽莉娜則因為從祁蓮聽上去就只是一句話的話語感受到了其居心之惡，而蜷縮著身體。
大概是因為被招至這個地方來的只有賽莉娜，我不過是多餘的付贈品吧吶。

「沒有，畢竟我總有一天必須得來伽羅瓦一趟的說」（大概是賽莉娜）

「妳這麼說真是幫大忙了哇。我在看到歐理維爾學院長的報告的時候，可是吃了一驚。如果只是打倒了魔兵們的話暫且不說，似乎妳還打倒了出現在恩特之森的大惡魔了呢」
「那並不是我一個人的力量。是因為克里斯汀娜桑和恩特之森的大家在才做到了的」
「是麼，真謙虛呢。就算這麼說，也還是了不起的事情哇。畢竟拉米婭雖然強大，但也絶不是能打倒大惡魔的種族。
我還以為在歐理維爾學院長或羽古多拉席爾的巫女姫出手，甚至是在羽古多拉席爾親自出馬之前，都無法擋住魔界者們的進軍吶。
然而看了揭曉的答案後，發現只是塞維斯特村和那近鄰一帶有所受害，就將魔界者們給擊退了，真的是吃了一大驚哇」
「哈啊⋯⋯」

摸不清祁蓮想要說的是什麼的賽莉娜，大多都是在嘆氣一般回應道。確實，很難想像對方特地地橫加插手管理官的工作把人招來了這所宅邸來了，就只是為了說這樣而已的話題。
對此我沒有插嘴，而是沉默地喝著紅茶潤喉，以及伸手向眼前放置在白瓷大盤中的茶點心。
嗚嗯，好吃。雖然好吃，但把這個拿來招待客人，感覺稍許有些用心過度了吶。
原來如此，這就是她招待客人的方式麼。

「那邊的小伙子似乎挺中意的呢。我希望妳也嘗一嘗味道。不論哪一個都是伽羅瓦的人氣食品哦」

聽從滿臉笑容的祁蓮的勸食，賽莉娜也仿照著我用手去抓眼前的食品，送入口中後不一會後，眼睛睜得圓溜。確實都是些離開都市後就無法品嘗到的美味。

「嗯，那個非常的好吃的說」
「是嗎，雖然我知道拉米婭的味覺跟人類相差無幾，但能合妳口味，我真的很開心哇。
話又說回來，到今天為止我也好幾次見到過拉米婭，但妳真的是漂亮啊。
並不僅僅是年輕，生氣磅礴而已哇。對活著的希望化作了活力和蓬勃朝氣令妳變得耀眼吶。
妳的黃金頭髮也是，青色的清澈眼眸也是，深綠色的鱗片也是，全部都非常地棒哇。這才是讓人想要吃掉的程度呢」

斷定這是開玩笑的話，她的眼中卻有著些許令人懷疑的光芒。
祁蓮繼續毫不吝嗇稱讚的言語稱讚著賽莉娜。一旁，賽莉娜雖然接受著祁蓮的讚美，但似乎是察覺到了從祁蓮言行的細微之處滲出來的險惡用心，害怕了起來。

「謝、謝謝您的誇獎」
「啊啦，開玩笑啦。嚇到妳了嗎？我並沒有其他什麼意思呦。是真的覺得妳很漂亮才這麼說的」

出神地嘟囔著的祁蓮，她的眼睛可以說完全忘卻了我的存在，只注視著賽莉娜，甚至讓人感覺她在來回撫摸著賽莉娜全身一般。

「祁蓮大人，賽莉娜她稍微有點兒怕生，您用這麼熾熱的視線看的話⋯⋯能不能還請您體諒一下」

我的救助讓賽莉娜安心了呼了一口氣。似乎沒有注意到在我插入談話的瞬間，祁蓮對我露出的僅此一瞬的惡意。

「是麼，那真是抱歉了。這是我的壊習慣吶」

是我的言語奏效了麼，在那之後的祁蓮控制住了自己對賽莉娜的露骨視線以及言行。
不過對我來說，她的態度不過是在掩飾自己罷了，所以我繼續在一旁不打草驚蛇地等著，看她何時才會摘下自己的假面具，現出原形。
一段時間之內，我們持續著關於伽羅瓦或總督府的閑談，但在這期間，舌頭變得無法很好地打轉，眼皮開始打起架來。
看來，摻合在招待我和賽莉娜的茶以及茶點裡面的毒，總算是開始發揮效果了。
嘛，我在嘗第一口的時候就已經察覺到了，但賽莉娜似乎並沒有發現的樣子。
最終，手指以及腳的感覺開始薄去，意識開始急速地遠去。
看著我和賽莉娜雙眼合攏，倚靠在椅子上的身體倒下後，祁蓮的臉上淨是笑容。只不過，那笑容之中含有的並不是理性，而是【惡意】二字。
祁蓮一鳴響傳呼鈴后，傭人們便就推進來了兩張帶有輪子的床。他們將沉睡中的我們被搬上了那個床上後，用皮帶將我們的手腳以及脖子和腰都給拘束住。
因為這些寫入有【魔力封印】術式的皮帶，我和賽莉娜別說使用什麼魔法了，甚至連不能編織成術式程度的魔力都沒辦法放出去。
一般這種情況應該可以這麼說吧：

──因為這個，我們兩人已經被完全的無力化了。

賽莉娜的暫且不說，畢竟是主角，我的話本以為自己會被猛地丟到床上，然後被亂七八糟地給捆住，但事實卻很是出乎意料，我是被很小心地搬到了床上的。
嘛，負責抬我的執事的手很冷很滑就是了，我都差點以為那是水在碰我。
就這樣被拘束在輪床上的我們，被運向了位於宅邸一樓中央的祁蓮的寢室去。（宅邸是三樓建築）

飄蕩著些微古書和藥品氣味的房間中擺放著一台沒有被使用過的暖爐，執事朝著那走了過去，手觸碰一個燭台。隨著他的動作，暖爐開始向左移位，露出了通向藏在其身後的地下室的通道。
含著刺激鼻孔氣味的潮濕空氣從通道中流出來，給我一種皮膚被黏上了的不快感。
我們在執事們的推運下，於走道拐了好幾道彎后，終於到了距離入口甚遠的橢圓形的房間內。
在房間的牆邊有著錬金術必須的巨釜以及巨爐、高度超過成年男性身高的巨大水晶，和垂釣在天花板的鈎子上被掛著的不知是來自什麼生物的肉塊，以及在巨大的玻璃筒中的綠色液體裡被浸泡著的各種生物標本。
地面上大大小小的魔法陣是用動物的血液或是融化後的礦物所描繪的。
從房間沾染了的魔力以及血腥味來看，我了解到這裡已經被使用了數十年了。
在房間的一角平排著驚人程度的巨大柴刀、鋸子以及剪刀等等。這些被膜得可以當鏡子用的物品，都是些三番五次又三番五次的沾染了血液，然後在每次使用後被膜復了的吧。
賽莉娜被運向房間的中央，我被留在相當於門口的地方，執事以及女僕們都靜候在牆邊，祁蓮則是用熾熱的視線盯著躺在輪床上的賽莉娜。

「嗚呼呼，雖然得手到了活著的拉米婭距離上次已經隔了很久了，但得到如此年輕美麗的拉米婭，還真是的隔了相當久吶。
更別說這還是擊退了魔界的魔兵以及大惡魔們的強大個體，對我來說這可是第一次。
賽莉娜，賽莉娜，嗚呼呼，沒白白地去強硬地拜託高達哇」
『啊，賽莉娜啊』

祁蓮如此熱情地低喃著，然後將手指伸向沉睡中的賽莉娜，這差不多快達到了我的忍耐極限。
啊啊，本還打算不管是怎樣的瘋子，只要還有藥可救就沉默地看到最後的，但現在還是算了。
猛地睜開眼皮的我僅僅動了眼珠子，將附加了威壓魔力的視線「砸」向了那樣靠近賽莉娜打算毛手毛腳的祁蓮。
祁蓮則嚇了一跳地一抖身體。

「藥的份量弄錯了嗎？居然就醒過來了。那麼重新向你道一次謝吧。
多虧了你的影響，她才吃下了點心，然後我的慾望才能夠將要得到滿足。
請放心。作為謝禮，至少就讓我給你一個痛快吧」

我從放置在這間房間的眾多拷問器具、四處黏著的血，犧牲者們的怨念以及描繪在地面上的魔法陣，大致知道了祁蓮曾經來此做過了些什麼。

（何等愚蠢之事吶）

「似乎妳在這做了不少聽了都覺得噁心的事情吶」
「可以說話了麼？是有附予抗毒耐性的魔法具嗎？呼呼，看來得先從把你的呼吸給止住開始才行呢」

回應祁蓮的眼神指示，在我不遠處的執事中的一人拿起來牆角的大斧子。身強體壯的大人似乎要用兩手握著才能揮舞的這個，到了執事的手中後，就跟在揮動樹枝一樣輕，揮舞起來毫不費勁。

「看上去挺熟練的樣子吶，殺了多少人了？」

對我的提問，祁蓮像是害羞了一樣撓了撓臉頰。

「誰知道，畢竟從一百以後就沒數了哇。兩百人⋯⋯不對，說不好是一千人？有可能還要多呢。
不過，數目上還要再加你一個人倒是很清楚。
來，請閉上眼睛吧。不用怕，疼痛似乎是一瞬的呦」

執事在我的一旁停了下來，為了一擊砍下我的頭而握著大斧子擺出大上段架勢。
鋒利度十足且具有重量感的斧刃，加上執事那從外表怎麼也想像不出的膂カ的話，一定可以很漂亮地砍落人類的頭的吧。

「損壊了的人造人們麼。雖說沒有自我意識，但有著這般無聊的主人，還真是可怜吶」

皮帶裡的魔力封印術式之類的，對我而言根本沒有任何的意義，畢竟我的魔力早就超出了其所能封印的極限了，而且還不是一星半點。
我用魔力強化完身體，「啪」的一聲將皮帶全數扯斷，將落來中途的大斧子接住，然後連帶著執事一起掄起來，頭朝下地猛砸向地面。
從執事那跟熟過頭的果實一樣被砸爛的腦袋中，紫色的腦漿和赤黑色的血液流了出來。

（血的顏色跟人類是一樣的麼，真是惡趣向）

「這可是驚人吶。要知道這是用古魯姆蜥蜴人的皮作成的皮帶呦。明明一百人來拉都拉不斷的說。果然是因為和魔兵們交戰過了，並不是什麼普通的人這麼一回事麼」

解開了拘束的我從輪床上下地，重新環視著這陰森森的室內。

「果然還是覺得無聊吶，妳是想做什麼才作出這樣的地方的？」
「並不是想做什麼了不起的事情哇。只是微不足道的研究而已呦。不管怎麼樣，隨著年輕的增長，身體也會變得越來越衰老不是？我則是沒辦法忍受那個吶。
即便是用魔法抑制也有著極限，所以就想乾脆換一個年輕的身體就好了嘛。
只要是年輕、充滿生命力的話，就算是人類以外的種族也各種各樣地換過了呦。為了那個，就連魔界的知識也利用了哇」
「Fumu⋯和死惡魔們簽訂了契約麼。不過，不管是收集契約所需的活祭品，還是搜集魔法的實驗體，應該都很花費精力的吧」

對於我的問題，祁蓮理直氣壯地回答道。

「那種東西，從平民那裡榨取的話，要多少有多少哇。
再說，不論是在哪一個時代，在哪一個国家裡，只要是有地位的人，總會有那麼一些無論如何都想要得到延命之術或是可以延長壽命的禁忌魔法物品的傢伙在呦。
他們把從平民百姓那榨取到的財富提供給我，對我研究中的犧牲睜一只眼閉一只眼。我則提供研究的結果，來回復他們的那些付出」

得意洋洋地說著自己的惡事，並對之感到自傲，真是無可救藥的女人吶。

「麻煩的傢伙則操控他的心，做成人偶桑就好了，而且總督府附屬魔法使這一身份真的是很便利哇。
愚蠢的權力者以及無知的民眾，多虧了這些無能的傢伙們才有了現在我吶」
「原來如此，典型的道路脫軌者麼。那麼的話，在恩特之森設置召喚的術式是惡魔的要求嗎？這地面的魔法陣跟恩特之森的召喚陣共同之處挺多的」

對我指出的地方，祁蓮『正解』地聳了下肩。那雙眼睛就像是看到了好學生的教師一般。

「觀察得挺仔細的嘛。話先說在前頭，那可不是我一個人做的事情呦。雖然繞過那個森林的警戒布置那一高度召喚陣並不是非常難，但我一個的話還是做不到的。
雖然除了我以外，似乎還有很多危險的魔法使們收到了邀請的樣子，但不湊巧，和他們並沒有什麼橫向聯系呢。
能送給你的冥府餞別禮就這麼多吶」

雖然並不知道她說的到哪裡是真，哪裡是假，但也已經沒有必要繼續聽後面那些更令人不爽的話了。

「是麼。賽莉娜，已經沒必要繼續裝睡了呦」

我判斷到祁蓮所言似乎並不是假的後，一邊扯掉束縛賽莉娜的皮帶一邊說道。

「啊，知道了。嗚嗚，有些難受的說」
「畢竟是為了封住行動的東西吶。不疼吧？」
「嗯，沒事」

賽莉娜從床上坐起身來，擦著自己被皮帶綁過地方，看到這一狀況的祁蓮嘆了口氣。

「這個藥一直都挺好用的，但現在看來還有改良的餘地吶」

這是我們事前就含在口裡的解毒劑──以瑪古爾婆婆直接傳授我的魔法藥知識調合出來的──發揮藥效了，再加上我從竜化中得到了耐性，賽莉娜則天生就有著抗毒耐性而有的結果。

「這是妳不懷疑自己的優勢地位，然後嘰嘰喳喳地多說了一大堆才變成這樣的」
「是麼，但只要你們沒有走出這間地上室，就什麼也不會變哇。只要把事情弄成我邀請來的拉米婭把同行的少年作為人質，突然暴動起來了的話，你們的結局要多少有多少呦？」
「我說妳說得太多了才變成這樣，可不是全在指我們兩個自由了的事。對吧，賽莉娜？」
「嗯！妳的所有企圖都已經讓高達桑和帕拉祢絲桑知道了的說呦」

賽莉娜這樣說道著，搖了搖掛在自己左耳上的風精石耳飾。這是高達在形踪被找到了的那天晚上給的魔法具，帶著這個的人所聽到的聲音，會被傳達到與這成對的另一只耳環上。
也就是說，我們和祁蓮之前的對話全部都已經給高達他們聽到了這麼一回事。現在這個時候，這間宅邸應該已經被高達和帕拉祢絲司祭手中有著的伽羅瓦士兵們與瑪依拉爾教的神官戰士們給圍住了吧。
實際上，我的耳朵也已經聽到了他們打破公館的大門，強行闖入後同人造人激烈交戰的騷亂聲了。

「是麼，這還是遺憾吶。明明還想著好不容易到今天為止都進行地挺順利的說⋯⋯」
「那可不好說。我可聽到從以前開始妳這傢伙就已經被當做嫌疑犯，握有關於妳一定的線索了。
就算不是今天，在不久的將來妳這傢伙也會被抓起來，然後被斷罪的吧。也就是說，妳這傢伙到底為止了」
「呼呼，被說到了這一地步，我反正心情變得輕鬆起來了哇呢。那麼這樣的話，就把你們和上面包圍著公館的傢伙們全部殺掉，然後隨便找個異国安頓下去吧」

比說還快，祁蓮徹底撕下掩飾的面具，從全身進發出殺氣，室內的執事和女僕們則手握著危險的凶器，完全是一副要宰了我們的樣子。
我將賽莉娜護在身後，拾起先前解決了的執事用的大斧子，與眼前的群敵們對峙著。
在我為了解決跑在前鋒的人造人女僕而右手單手揮起大斧子的時候，被斬成兩半的執事們的屍體從地下室的入口處掉了進來。

「賽莉娜，多蘭，你們沒事吧！？」

帶著震動地下室般的大聲音突然闖入地下室的人，是克里斯汀娜。
克里斯汀娜毫不留情地砍殺著擋路的人造人，全身上下都進發著鬥氣和魔力以及怒氣。
跟在我和賽莉娜離開貝倫村的時候，克里斯汀娜她自己所宣言的一樣。通宵地全力奔馳在貝倫與伽羅瓦之間的她，比我們還要早上不少到達了伽羅瓦。然後在第一時間從歐理維爾那得知了事情原委，強硬地參加了這次的祁蓮逮捕行動之中。
那天晚上，高達、帕拉祢絲司祭和一同在的歐理維爾，這三人來到我和賽莉娜在的地方，跟我們說明了各種事情。
原本伽羅瓦那邊並沒有把賽莉娜的事情問題視到之前說的那一地步，但從很久以前就被視為危險人物監視著的祁蓮，不知為何插手了這一件事。於是判斷到這是抓住祁蓮尾巴的好機會的總督府，在此制訂了在帶賽莉娜來伽羅瓦的同時捕捉祁蓮的計劃。
在來自對祁蓮表示懷疑的總督府內的一部分人的指令下，接受了總督府委託的歐理維爾和高達以保證我和賽莉娜的安全以及謝禮作為交換，打算在途中用此來拜託我們去做祁蓮自我招供的事情，但在帕拉祢絲司祭和跑回到伽羅瓦的克里斯汀娜兩人的介入下，變成了現在的這副模樣。
克里斯汀娜參戰的事情，是從那天晚上後也還有著聯繫的歐理維爾那被告知的。

「克里斯汀娜桑！請不要太亂來了，很危險的說啊！」

對順利的大步向前邁進的克里斯汀娜，賽莉娜發出了驚訝且擔心的聲音。
現在的克里斯汀娜正把自己的事情放在第二位，為了確認我和賽莉娜是否真的沒有受傷，一心向前，對高舉大劍猛襲來的執事僅僅是捕捉氣息地揮動艾祿斯帕達，將之上下兩分。

「雖說能聽到你們的說話聲，但還是感覺坐立不安啊。你們兩個沒事真的是太好了」

不管怎麼說，在克里斯汀娜發自內心地擔心著我們一事上，沒有任何的虛假。如此地珍視著我們，真的是感激不盡吶。
我用魔眼往上方仰望去，看到在克里斯汀娜後方，武裝好的士兵和神官戰士們早已突入了公館中，正開始著和人造人以及石頭人們的戰鬥。

「怎麼說也不可能是克里斯汀娜桑一個人來吶，但就算這樣，也還真是亂來吶」
「亂來了的是你們兩個！明明當誘餌會發生什麼都不知道，你們還來！」

克里斯汀娜所說的正確無比，所以我和賽莉娜完全想不出反論的言語，但從地下室入口傳來的聲音維護了沉默下去的我們。

「並不是應該那樣責備的事情哦，克里斯汀娜。若是擊退了墮神和大惡魔的他們的話，這種程度的事情還沒到稱為危機的地步對吧。而且事實上，他們也沒受什麼傷的說呢」

新登場的人物是擔任魔法學院學院長的樹精靈歐理維爾。身著著在恩特之森那會相同的袍子，討伐魔兵們那會帶著的杖也同樣持在手中。
歐理維爾那雙就像是完全看不到情感的冰酷眼神看著祁蓮。一邊，祁蓮雖因克里斯汀娜的過度美麗而看得出神，導致現在發聲也給忘了一樣，但歐理維爾的出現似乎也是她預料之外的事情，所以也微微地睜大了瞳孔。

「真是沒想到居然連妳都現身了。不對，這反倒是個好機會吶。能將令人感覺只應天上有的美麗人類女子，和繼承著古老血統的高級精靈都給入手的機會到了面前，我還真是幸運啊」

對帶著猙獰和粘著質的執著心一同笑著的祁蓮，在歐理維爾之後，克里斯汀娜也冷酷地看著她。

「祁蓮，雖然妳是一名優秀的魔法使，但做了如此邪道的事情，也是沒什麼可以狡辯的吶。
乖乖地束手就擒我是不會說的。請隨妳喜歡地盡情抵抗吧。那樣的話，我就可以毫無顧慮地把妳打得再也站不起來了呢。
姑且，命我還是會幫妳保住的，畢竟讓妳經歷了想要說『求求你殺了吧』的經歷後，拷問要花的工夫也就可以省了呢」

對一臉豪爽地說著不得了的事情的歐理維爾，賽莉娜驚訝地合不攏嘴，我和克里斯汀娜則『就該這樣』地表示完全贊同。
這大概是教育的不同吧。

「嚯嚯嚯，傲慢的死高等精靈（Elf）。甚至連我是從何時活到現在的都不知道，就這樣誇下口海。請看吧，這是吾花費三百年鑽研的成果呦！」

祁蓮並沒有因為歐理維爾的言語而變得激動，用手一口氣撕掉了身上的禮服，露出了硬塞在裡面的異性之姿。她那變得不再需要隱藏的身體在我們的眼前肥大化，差一點點就到了地下室的天花板。
生有剛毛的蜘蛛之足和持有巨大吸盤的章魚之爪，有著凶惡爪子的猛獸四肢，更有蛇與蛙、禿鷹與蜥蜴、螳螂和飛蛾、蜂等各種各樣的生物部位在蠢蠢欲動著。
這些都被無節操的接合在一起，成為了祁蓮巨體。然後在這些的中間，還保有膝蓋以上、勉勉強強還殘有人類面影的祁蓮在那。
魔界的技術和眾多的生物肉體都採用了的結果，就是如此醜陋至極的愚蠢模樣。

「請看吧，我這融合了眾多的生命，比那些都要巨大，比那些都要強大，比那些都要美麗的身姿！
我與存於這個世界的一切生命合為一的同時，永世長存了呦。我永世長存了，也就是因我而犧牲那些生命也永世長存了。所有的存在都應該為成為了我不老不死的食量而喜悅哇。
至今為止的那些生命是如此，從今以後的也是如此，然後你們這些傢伙們也是！
請和我一同獲得永遠的美與生吧。這就是我給予這個世界上的愚民們的慈悲啊」
「丑爆了吶。甚至再看上一眼都感覺超不爽吶」

對我毫不留面子吐出來的言語，其他三人也點頭表示贊同。
同我在前世目擊到厭倦了的那些讓慾望肥大化，最終變得無法控制慾望、位於傻子巔峰的傢伙們是一樣的吶。誤解了只是勉強算是優秀的自己的才智，然後在輸給了慾望後做出了如此之蠢的事情來。

「不論哪個時代的凡夫俗子都不理解我這至高的思維啊。來吧，大家都同等地化為我的血肉，和我成為一體吧。這是拘束住永遠的至高幸福啊」

祁蓮的眾數之足一蹬地面，絲毫不在意被自己踏碎的執事和女僕們化為了赤血之花，氣勢洶洶地向我們猛襲過來。
製作這些執事人造人的一部分材料，應該也是利用了祁蓮手上的犧牲者們吧。何等可悲吶。
祁蓮的身體在發出肢體斷裂地滲人聲音的同時縱向裂開後，立馬從那裡放出了不計其數的白絲。
蜘蛛絲？  不對，從觸碰到了其中一根的女僕在眨眼之間便就消融掉了來看，那些並不是普通的蜘蛛絲。是把強酸弄成了絲狀物嗎？
最先動的是歐理維爾。她輕輕揮動杖後，短暫地影響了風之精靈。

「風啊，請切裂微塵，拭去不淨吧」

嵌（Qian）入在歐理維爾攜帶著的杖上的風精石一發光後，於空中呈放射狀展開的絲就被切斷為百萬之多的斷片。為了讓那些不落下我們，風將那些吹向和我們所之處不同的方位。

「我來止住她的行動！」（賽莉娜）

雖然處理完了酸之絲，但還在往這邊襲來的祁蓮並沒有停下來。這一次出列的是賽莉娜，她絲毫不見畏懼之色地與祁蓮對峙著。將魔力集中在眼瞳上，發動了持有麻痺之力的蛇之魔眼。

「停下！」
「呵，拉米婭的魔眼，可這也強力過頭了」

賽莉娜每日都吸取了我竜種的精氣，她的魔眼也隨之日益高漲著力量，現在早已超過了祁蓮的魔法耐性，就算是以巨大質量進行衝擊的事物，也漂亮地給你停下來看。
在賽莉娜全力以赴地阻住祁蓮的期間，克里斯汀娜一邊用愛劍斬除礙事的執事或女僕，一邊奔跑起來。

「居然想傷害我的朋友，罪該萬死吶！」

正面感受著克里斯汀娜放出的怒氣祁蓮顯出了畏懼，為了雪洗自己那樣的醜態，她開始了反擊。她從屬於我們視線盲區的自身背後，伸出前端有著鐮狀之刃的觸手和昆虫的足來迎擊克里斯汀娜。

「殺啊啊啊啊啊！！」
「雕虫小技！砍了她，艾祿斯帕達！」

克里斯汀娜對向自己襲擊來的刃，以僅可看見閃光的神速斬擊將之盡數斬斷。觸手和虫之足一邊灑著五彩的體液，一邊撲通撲通地落在克里斯汀娜的周邊。

「咕呵呵呵，疼、疼、疼──呀噫噫噫噫噫噫！竟敢把我的身體，我的腳給這樣！妳、妳、妳這死丫頭啊啊啊啊啊啊啊啊啊啊啊啊」

對祁蓮口沫飛吐著的尖叫，我微嘆了口氣。

「果然吶。由於用拙劣的技術，不斷強硬地把異種族的肉體融合在一起，導致精神和靈魂正演奏著不和諧之音。我應該說一句真虧妳能維持到今天吶，但現在保持清醒也快到極限了吧」

在祁蓮的身體中各處，除了和她自身所有的嘴大小度不同之外，其他都完全相同的口張開，那些都在露出白齒紅口內的同時，一齊發出悲鳴。
在我們因太過刺耳的悲鳴而都皺起各自的眉頭時，轉身回首望來的祁蓮目不轉睛地盯著我。發瘋之中主要瞄準了我麼。

「悲嘆之聲低喃～～，魔界支配者啊、埃爾基塔啊！」

Fumu？名字雖然不耳熟，但從名字上感受到的言靈來看，這是比大惡魔還要高位的爵位級惡魔嗎。沒想到到了這個地步，居然還有借助惡魔的力量只行使暗黑魔法的判斷力。
可是，能將小城崩壊掉的強力暗黑魔法居然在現在這樣的狹小地下室中使用，是做好了自滅的心理準備麼？

「以怨嗟之悲鳴為魔界門之玥（Yue），啟破壊世界之渦吧。伊達Ｏ集Ｏ阿斯塔！」

【暗黑魔法】，與理魔法和精靈魔法、拉米婭種的固有魔法都不同，是一種還應該被稱為邪惡奇跡的魔法。
現在，借爵位級惡魔之力聚集起的龐大負之魔力，正化作奔流向我洶涌而來。
但是，爵位級的惡魔我早就已經殺順手了吶，更何況這只不過是借其之力使用的暗黑魔法而已，根本入不了我的法眼。
我對這迫近來的暗黑之力，以放出純魔力奔流的理魔法從正面進行互相傷害。

「力之理啊   遵從吾之聲   化作擊碎吾所有敵人的奔流   Energy Blast（エナジーブラスト  能源沖破）！」

從我抬起來的左手放出的綠光奔流和暗黑奔流互相衝撞，展開了一進一退的攻防，但這種僵持不下的狀態也只是一瞬的事情。
從起源保有的魔力量和我與祁蓮之間魔力操作能力，由於這兩者的絶對差距，【Energy Blast（能源沖破）】不費吹灰之力便貫穿暗黑奔流，然後就那樣直接擊中祁蓮，將醜惡的肉體大部分給吞沒、消滅。

「呀啊啊啊啊啊、啊啊啊啊啊！？疼啊疼啊疼疼好疼啊疼啊啊啊啊──」

失去了膝蓋之下的大部分異性肉體，全身升騰著白煙的各處被碳化掉的祁蓮，在地面上一邊喊著疼一邊來回打滾。
我則在走近那樣的祁蓮的同時，繼續發動【Energy Blast（能源沖破）】砸向基本上已經戰鬥不能的她。

「噫噫、噫噫噫噫────」
「妳這傢伙在自己所殺之人喊疼的時候，你這傢伙怎麼回答的？對向妳這傢伙祈求慈悲，求妳給一個痛快的者們，妳這傢伙又是怎樣回答的？
這雖然只是冰山一角的冰山一角，但也是妳這傢伙至今為止的所作所為得到的報應吶，給我在疼痛和恐懼中理解去吧」

我將力量調節到絶對不會殺死祁蓮的程度，毫無間隙地持續折磨祁蓮。注視著這副模樣的歐理維爾發出了制止的聲音是在經過了一段時間之後的事情。

「請就到此為止吧，多蘭。你的憤怒我明白，但再繼續下去祁蓮的精神就維持不住了吧。
雖然她知道的可能並不是太多，但也是貴重的情報源頭。我想要她繼續發揮餘熱的說」
「十分抱歉。稍稍有些氣血上頭了」

在我因歐理維爾的制止而停下魔法的時候，祁蓮已經沒法再說話，僅僅還有身體微微抽搐著可以當做她還活著的證明。
總有一天，在祁蓮死了後，其魂定會被送至冥界，接受與生前所行相應的懲罰的吧。畢竟，冥界眾神對死者在生前所犯之罪，會降下嚴正的處罰。現在還不是順著怒火把這個罪人葬送掉的時候。

『祁蓮啊，到時妳在冥界一邊後悔己身所犯愚行之多，一邊接受折磨去吧。地獄的獄卒們一切寬恕之行都不會給你的吶』
「那麼，現在可以認為姑且已經把在伽羅瓦要做的麻煩事解決掉了大部分了不？」

對這樣詢問道的我，歐理維爾點了點頭。

「嗯，雖然給你和賽莉娜帶來了很大的麻煩，但這樣一來高達管理官和伽羅瓦總督府也就能夠接受了吧。而且，和祁蓮有所關聯的愚者們也能夠一齊處分掉了。
至於贈予你們的謝禮，我想包括賽莉娜居住的貝倫村的手續在內，應該會準備得很足量的。如果不是的話，我和帕拉祢絲司祭會對他們發出嚴正的抗議的。而且，也請收下來自我個人的道謝吧」
「您能這麼說就真是太好了。至少覺得沒白白地以這種形式來伽羅瓦一趟了」

在我們這邊結束後沒過多久，公館也被總督府的士兵們和瑪依拉爾教的神官戰士們給鎮壓完畢了。我們則給變得跟破爛一樣的祁蓮施加了好幾重的封印，確認捕捉完畢後，離開了這間反反覆覆地進行過了噁心行徑的公館。

＊    ＊    ＊

在捕縛祁蓮的騷動之後，我們暫借了歐理維爾所使用的一間宅邸，一直到賽莉娜的居住手續為止都逗留在伽羅瓦。
雖是這麼說，但由於總督府明面說是護衛實則為監視而派來的士兵的緣故，導致我和賽莉娜並不能自由地外出宅邸。
大概是由於祁蓮一事的事後處理的關係，我們被限制了和外部的接觸，最多就是和關心著我們而來訪的克里斯汀娜聊天，以及和被派來的公務員進行居住手續的辦理而已。
除此之外的時間，都是在宅邸的書房裡瀏覽群書、書寫抄本來度過，難得來到了伽羅瓦卻連一次觀光都不能去，實在是非常遺憾。
時間流逝，沒過幾天一系列的手續就完成了。在我和賽莉娜準備起身回貝倫村的早上，高達管理官和歐理維爾來訪問我們，就一連事件的來龍去脈，跟我們說了一次。
被我們請進會客廳的高達管理官，開口的第一句就是向我和賽莉娜表示歉意，低下了頭。雖然他在貝倫村的時候做出了副高姿態的模樣，但現在的態度卻像是另一個人一樣，對此，我和賽莉娜兩臉懵逼。

「關於今回之事真的是給你們添麻煩了，非常抱歉」
「沒，管理官大人也有著自己的立場對吧，而且結果上來看我和賽莉娜也沒有受傷，還請您抬起頭來」
「不不不，那可不行。把應該守護的子民卷入了這樣的事情之中，身為管理官應對此感到羞恥。
更何況，與此事相關的還是附屬總督府的魔法使，不但如此之長的時間任之為所欲為，還有確實有著複數的幫助其行惡的鼠輩在，不論是對国王陛下還是對民眾們，都無容以見。
與祁蓮那東西相關的惡事已在王国深根蒂固。要將那些全部除去還需要一定的時間的吧。
抱有清廉之志的人也在長時間的與名為權力的魔物打交道中，不知何時之間居然忘卻了自己心中的志向。真是可悲吶」

高達以自己完全沒有可辯解的地步的態度，向這邊低下了頭。

「哦多，不行吶，淨是說些負能量的話題了。再就是，老夫重新說一遍吧，當然約定好的謝禮一絲也不會差。
拉米婭小姑娘的居住手續也平安無事地辦理完了。在這以後就可以昂首挺胸地居住在貝倫村了。
再者，這是老夫個人的感激之情。雖不過是市內有名的點心而已，但你們也拿去在回去的路上吃吧」

這樣說道的高達，以誇張的動作取出了四方的盒子。嘛，能得到的東西我是來者不拒就是了。

「那個，跟多蘭桑說的一樣，還請您不要太在意了。畢竟壊的是那個叫做祁蓮的魔法使桑，管理官桑您只是在完成自己工作而已」
「噢噢，何等溫柔的言語。賽莉娜君，妳真的是一位心靈善良的拉米婭啊。這樣的話，在貝倫村的生活中也就不用多操什麼心了吧」

啊啊，眼前的這個男性真的和在貝倫村看到的高達是同一個人嗎？工作的時候和沒工作的時候的態度不是相差地太多了嗎？

「感覺這個話題怎麼也說不完的樣子呢。話說回來，管理官大人，我有一件想要問您的事情，不知⋯⋯」
「何事？是老夫能回答的事情的話，一定回答」

實際是以高達的來訪為起端的這一連事件，給至今為止都打算含糊地在貝倫里度過一生的我，帶來了思想上的轉折點。
站在解決了這次事件的立場上的我們，現在已經暴露被治理一側的祁蓮以及幫助其行事者的惡意之中。
我和賽莉娜的話，因為有著能夠對抗那些惡意的力量、能得到想要阻止那些的人們的協力，倒也還好說，但大多數的人都沒有我們這樣的力量和人緣關係的吧。
如果在我不在的時候，賽莉娜或者是貝倫村裡的誰遭遇了這次一樣的事情，然後變得為時已晚了的話⋯⋯一想到這裡，我就覺得後背一陣陣發冷。
且就算不會發生那樣的事情，關於官員的不公正或腐敗苦了百姓的話題，回首古今，十分可悲的要多少就可以找到多少。雖然貝倫村現在還沒有變成被蠻橫榨取的對象，但卻不敢保證今後會不會變成那樣。這一次可以把話題丟在一邊就此跳過，但下次的話就不能這樣了吧。
通過這次的事件，我很清楚地明白了管理者的心性如何，左右著我們這些被管理者們的生活，甚至有時還會導致這邊被強以施加難以接受的蠻橫無理之事。
那麼的話，為了今後不會遇上那樣的事情，該怎麼做才好？
這個問題的答案之一，直白地來說的話就是自己成為管理的那一端。為了守護多數的人，我必須得要去取得現在自己還未持有的、能在人類社會中發揮的那些力量。
我自重生為人類以來，第一次懷有著就算要花一段時間，也要離開村中的耕田以及與家人一同的生活的想法。

「這回一樣的事件不可能只有這一次，請問那個祁蓮一樣的危險分子，有多少在伽羅瓦裡？或者說，全国上下到底有多少那樣的傢伙在呢？」
「嗚姆，關於這個是屬於機密的，所以不怎麼好回答吶。老夫自己的地位並不是手握重權的吶。
因發覺了此次問題之故，總督府開始了內部的重新審查。如今之時，在老夫所知範圍內並無祁蓮那般顯眼者在，要抓住惡黨之尾並非簡單之事。
畢竟情況很複雜，不顧国家、在暗中活動者也有著吶。說不定在吾等無法想到之處也存在著出謀划策者。從今以後也有可能會發生超過吾等最大限度警戒之事啊」
「是這樣麼。那麼，歐理維爾大人，請問恩特之森那邊如何？設置在森林裡的召喚陣應該不只有我們發現的那一個」
「已經確認完了森林全域，其他的召喚陣也在發現後進行了徹底破壊或施加封印。
而且各部族的聯合以及與森林的意志交流也很活躍，將空間本身固定化的大規模結界也展開了。首先可以斷言外部者再次侵入森林中進行設置魔法陣是不可能的。
為了杜絶再次招來那樣的危機而導致連你的故鄉也會被波及到的事情發生，森林那邊正在全力戒備著」
「這樣啊，這還真是令人開心的消息。其實，我在前些日子之前，都一直認為在故鄉裡度過一生是最好的。雖然現在也沒有變，但關於該怎麼度過這一點，我覺得我或許應該稍微地改變一下想法了」
「吼，那又是怎樣的？」

我直視著高達和歐理維爾，說出了心中的話。

「到現在為止，我一直都認為自己當一個普通的農民過完一生就行了。但現在的話是不是除此之外還有著道路可以走。然後就想到了以『從被管理者成為管理者』為目標不也是一條路麼。
或許這會讓您覺得這是非常狂妄的事情而發怒，但我還是認為再發生這次一樣的事情的時候，不僅僅是單純的武力，那些也是保護故鄉必要的力量之一」
「嗚姆，一般情況都是為了出人頭地或是為了得到榮譽才選擇走仕途的，你的話最多就是為了把那變成手牌而已麼。
確實是狂妄的事，但⋯⋯嗚姆，多蘭君，通過恩特之森一事以及此次祁蓮一案，總督府內都已知曉你是身懷著非凡的魔法才能之人。
雖然老夫並不知道你能夠爬至何等高度，但成為總督府附屬魔法使應該不需多少時日的吧。
不過你若是稍稍繞點原路的話，有著離飛黃騰達更近之途」

這樣說道的高達看了一眼鄰座的歐理維爾──伽羅瓦魔法學院學院長的高等精靈。

「是說入學伽羅瓦魔法學院嗎？」（應該是多蘭）

「沒錯。雖老夫這麼說有點難聽，但現在的你一切後盾皆無。不過，若是你在歐理維爾殿擔任學院長的魔法學院學習的話，不僅僅是總督府，王国北部的各種貴族與大商人都會注意到你的吧。
那樣的話，你所求的力量，嘛，換而言之就是權力、財力、人脈，這些東西的上升機會定然會獲得的。
不知您意下如何吶，歐理維爾殿？」
「您們說的我很是明了了。老實地說，今天我本來就是為了探問多蘭是否想要入學而來訪的，對我來說這正是恰好。
管理官殿應該不知道，在我學校裡任職的丹澤爾教師和多蘭是同門師兄弟，他從很久以前就向學校推薦過了多蘭的入學。
只不過多蘭他至今為止都不願意離開自己的故鄉，所以並沒有入學，但要是多蘭自己希望入學了的話，我就笑納了這次能夠入手有著如此優異素質學生的機會吧」
「噢噢，是這樣子的啊。若是恩特之森的戒備也已萬全了的話，即便再次發生不久之前的那般異變，大概也不會牽連至貝倫村的。
如此一來的話，多蘭君也就可以安心地離開故鄉，入學魔法學院了。嘛，話雖如此，但伽羅瓦與貝倫村之間單程一次都不用花一周時間」
「當然，入學的時候要接受考試哦。另就是，你的編入要稍微晚一點呢」
「嗯，這個我明白。我也想回村子一趟，跟家裡人還有瑪古爾師傅通一聲氣，至少要通過丹澤爾桑跟他們說一下我入學的事才行」
「這樣啊，那麼我就等著丹澤爾教師的聯絡吧」

這樣說完後的歐理維爾端起面前的茶杯喝了一口茶，在一旁默默地聽著我們交談的賽莉娜，則一臉不安的模樣偷偷看著我。
畢竟邀請賽莉娜到貝倫村去的我，現在想要離開貝倫村去入學伽羅瓦魔法學院了。雖然這是很不合情理的事，但只要賽莉娜同意的話，我確實有辦法帶著賽莉娜一起入學伽羅瓦魔法學院。
雖然明明好不容易辦理完了賽莉娜居住貝倫村的手續，卻又要立刻離開了村子什麼的，真的很難不然人覺得不是白忙活一場。但我的計劃是在畢業後就立刻回到貝倫村，所以即便賽莉娜和我一起去了魔法學院，先前辦完的居住手續倒也不會是白辦。
也就是說，現在問題就只有取得家裡人和師傅瑪古爾婆婆，以及賽莉娜的同意這一點了。
在這之後，高達和歐理維爾兩人因為還留有工作要做而告辭了，等著他們離去的賽莉娜則現在也是一副即將哭出來的表情，問向我。

「多蘭桑是打算離開貝倫村嗎？多蘭會離開貝倫村什麼的，我從來都沒有想過?・°（??﹏？？）°・？⋯⋯」

賽莉娜現在已經是一副眼淚汪汪的模樣了，流下大粒的眼淚也只是時間問題。

「對不起。明明是我邀請賽莉娜到了村子裡來，我卻還任性地說要離開村子，真的對不起」

我至少應該在跟賽莉娜通一聲氣之後才跟歐理維爾她們說這個話題的，我啊我，到底做了何等欠缺考慮的事情啊。
對深深地彎下腰、低下頭的我，賽莉娜拚命地忍住要顫抖的聲音，請我抬起頭來。

「請、請抬起頭來。多蘭桑有多蘭桑想做的事情，不需要為了我而忍讓。
要是多蘭桑不在的話，老實來說我會、非常、非、非常地寂寞，但是我會克服的」
「賽莉娜⋯⋯」

聽到惹人怜愛的賽莉娜所說的話後，我的心情變得非常地難受。在我找不到言詞，不知道接下來該怎麼跟賽莉娜說的時候，賽莉娜突然以一副與之前為止的表情有著稍許不同的表情，戰戰兢兢地詢問向我。我感覺到，賽莉娜的心中有著某種非常強的決意被堅定了。

「那個，多蘭桑。如果，如果我說我想要和多蘭一起來伽羅瓦的話，我可以一起來嗎？」
「昂，只要賽莉娜妳願意的話，我確實有且只有一種方法能讓我們同行。這是我以前跟丹澤爾桑問到的，但並不是什麼值得讚賞的方法⋯⋯」

我含糊其辭躊躇著的時候，一副已經下好了決心樣子的賽莉娜兩手握拳，向前探身，強且有力地說道。

「Wo、我覺得跟多蘭桑在一起就好！所以，如果多蘭桑要去伽羅瓦的話，不管怎樣，我都絶對要一起去！！」

對憋紅著臉如此宣言了的賽莉娜，我不自禁地揚起了嘴角。然後，我將自己坦率的心情告訴了賽莉娜。

「謝謝妳。不得不離開賽莉娜我也很寂寞，所以妳能這麼說我非常的開心啊」

理解了我所說的賽莉娜，一直發紅到了耳尖，低下了頭。
啊啊，真是的，賽莉娜真的是可愛吶啊。